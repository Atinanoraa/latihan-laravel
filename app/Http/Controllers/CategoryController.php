<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    function category(){
        $data_category = Category::all();
        return view("category-list")
        ->with("data_category", $data_category);
    }
    function create(){
        return view("category-create");
    }
    function save(Request $request){
        $data_category= Category::create([
            "kategori" =>$request->input("kategori"),
            "jumlah" =>$request->input("jumlah"),
        ]);
        if($data_category){
            return redirect(url("category"))
            ->with("status", "sukses");
        }
        else{
            return redirect(url("category"))
            ->with("status", "gagal");
        }
    }
    function edit($id){
        $data_category = Category::find($id);
        return view("category-edit")
            -> with ("data_category", $data_category);
    }
    function update($id, Request $request){
        $data_category = Category::find($id);
        $data_category->kategori=$request->input("kategori");
        $data_category->jumlah=$request->input("jumlah");
        $data_category->save();
        return redirect(url("category"));
    }
    function delete($id){
        $data_category = Category::find($id);
        $data_category->delete();
        return redirect(url("category"));
    }
    
}
