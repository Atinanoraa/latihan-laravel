<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    function produk(Request $request){
        $search = $request->input("search");
        $ordering=$request->input("ordering");
        $urutan= $request->input("urutan");
        $filter=$request->input("filter");
        $go=$request->input("go");
        
        if($go=='1'){
            $data_product=Product::where('nama', 'like', '%'.$search.'%')
                                    ->orderBy('nama', 'asc')
                                    ->paginate(3);
        }
        else{
            $data_product=Product::paginate(3);
        }
        if($filter=='1' && $ordering != '' && $urutan != ''){
            $data_product=Product::where($ordering =='nama')
                                    ->where( $urutan == 'asc', 'desc')
                                    ->orderBy($ordering, $urutan)
                                    ->paginate(3);
        }
        // elseif($filter=='1'){
        //     $data_product=Product::where($ordering != '')
        //                             ->where($urutan == '')
        //                             ->orderBy($ordering, 'asc')
        //                             ->paginate(3);
        // }
        // elseif($filter=='1'){
        //     $data_product=Product::where($ordering == '')
        //                             ->where($urutan != '')
        //                             ->orderBy('id', $urutan)
        //                             ->paginate(3);
        // }
        else{
            $data_product=Product::paginate(3);
        }
        return view("product-list")
        ->with("data_product", $data_product);
    }
    
    function create(){
        return view("product-create");
    }
    function save(Request $request){
        $data_product= Product::create([
            "nama" =>$request->input("nama"),
            "slug" =>$request->input("slug"),
            "menu_order" =>$request->input("menu"),
            "status" =>$request->input("status"),
        ]);
        if($data_product){
            return redirect(url("product"))
            ->with("status", "sukses");
        }
        else{
            return redirect(url("product"))
            ->with("status", "gagal");
        }
    }
    function edit($id){
        $data_product = Product::find($id);
        return view("product-edit")
            -> with ("data_product", $data_product);
    }
    function update($id, Request $request){
        $data_product = Product::find($id);
        $data_product->nama=$request->input("nama");
        $data_product->slug=$request->input("slug");
        $data_product->menu_order=$request->input("menu");
        $data_product->status=$request->input("status");
        $data_product->save();
        return redirect(url("product"));
    }
    function delete($id){
        $data_product = Product::find($id);
        $data_product->delete();
        return redirect(url("product"));
    }
}
