<?php

namespace App\Http\Controllers;

use App\latihan;
use Illuminate\Http\Request;

class LatihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\latihan  $latihan
     * @return \Illuminate\Http\Response
     */
    public function show(latihan $latihan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\latihan  $latihan
     * @return \Illuminate\Http\Response
     */
    public function edit(latihan $latihan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\latihan  $latihan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, latihan $latihan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\latihan  $latihan
     * @return \Illuminate\Http\Response
     */
    public function destroy(latihan $latihan)
    {
        //
    }
}
