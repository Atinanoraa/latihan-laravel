<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table ="latihan__product";
    protected $guarded=[];
    public $timestamps = false;
}
