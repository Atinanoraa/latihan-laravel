<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action="{{url('product/save')}}" method="POST">
        @csrf
    <table align="center">
        <h1 align="center">FORM PESANAN</h1>
        <tr>
            <td><label for="nama">Nama</label></td>
            <td><input type="text" name="nama" id="name" placeholder="masukkan nama"></td>
        </tr>
        <tr>
            <td><label for="slug">Slug</label></td>
            <td><input type="text" name="slug" id="slug"></td>
        </tr>
        <tr>
            <td><label for="menu">Menu Order</label></td>
            <td><input type="text" name="menu" id="menu"></td>
        </tr>
        <tr>
            <td>Status</td>
            <td><input type="radio" id="aktif" name="status" value="online">
                <label for="aktif">Aktif</label><br>
                <input type="radio" id="nonAktif" name="status" value="offline">
                <label for="nonAktif">Non Aktif</label><br>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><button type="submit" href="product/save">Simpan</button></td>
        </tr>
       
    </table>
</form>
    
</body>
</html>