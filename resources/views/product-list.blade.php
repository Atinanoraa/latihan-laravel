<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <title>Restoran</title>
    <style>
      tr:nth-child(even){
        background-color: aqua;
      }
      td{
        text-align: center;
      }
      .table{
        border: 1px solid black;
      }
      .thead{
        color: aliceblue;
        background-color: darkslategrey;
      }

    </style>
  </head>
  <body>
    <ul class="list-group">
        <h1 class="text-center font-weight-bold">MENU MAKANAN</h1>
        <div class="container">
          <form action="{{url('product')}}" method="GET" class="mt-2">
            <input type="text" name="search" placeholder="search... ">
            <button type="submit" value="1" name="go" class="fa fa-search">SEARCH</button>
            <select name="ordering">
              <option value="">Urutan Berdasarkan</option>
              <option value="nama">Nama</option>
              <option value="menu_order">Menu Order</option>
            </select>
            <select name="urutan" id="urutan">
              <option value="">Urutan</option>
              <option value="asc">ASC</option>
              <option value="desc">DESC</option>
            </select>
            <button type="submit" value="1" name="filter" class="fa fa-search">FILTER</button>
          </form>
          <table class="table table-bordered mt-3">
            <tr class="thead">
              <td>NO</td>
              <td>NAMA</td>
              <td>SLUG</td>
              <td>MENU ORDER</td>
              <td>STATUS</td>
              <td>ACTION</td>
            </tr>
            @foreach ($data_product as $row )
            <tr>
              <td >{{$row->id}}</td>
              <td >{{$row->nama}}</td>
              <td >{{$row->slug}}</td>
              <td >{{$row->menu_order}}</td>
              <td >{{$row->status}}</td>
              <td>
                <a href="/product/edit/{{$row->id}}" class="btn btn-primary">Edit</a>
                <a href="/product/delete/{{$row->id}}" class="btn btn-danger">Delete</a>
              </td>
            </tr>
            @endforeach
            
          </table>
      </div>
    
    <div class="text-center mt mt-3">
        <a href="/" >Home</a>
        <a href="/category">Category List</a>
    </div>
    {{ $data_product->appends(Request::all())->links() }}
    
    
   
   
    
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    -->
  </body>
</html>