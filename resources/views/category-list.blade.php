<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <title>Restoran</title>
    <style>
      tr:nth-child(even){
        background-color: aqua;
      }
      td{
        text-align: center;
      }
      .table{
        border: 1px solid black;
      }
      .thead{
        color: aliceblue;
        background-color: darkslategrey;
      }

    </style>
  </head>
  <body>
    <ul class="list-group">
        <h1 class="text-center font-weight-bold">KATEGORI</h1>
        <div class="container">
          <table class="table table-bordered">
            <tr class="thead">
              <td>NO</td>
              <td>KATEGORI</td>
              <td>JUMLAH</td>
              <td>ACTION</td>
            </tr>
            @foreach ($data_category as $row )
            <tr>
              <td >{{$row->id}}</td>
              <td >{{$row->kategori}}</td>
              <td >{{$row->jumlah}}</td>
              <td>
                <a href="/category/edit/{{$row->id}}" class="btn btn-primary">Edit</a>
                <a href="/category/delete/{{$row->id}}" class="btn btn-danger">Delete</a>
              </td>
            </tr>
            @endforeach
            
          </table>
      </div>
    
    <div class="text-center mt mt-3">
        <a href="/" >Home</a>
        <a href="/category">Product List</a>
    </div>
   
   
    
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    -->
  </body>
</html>